jQuery(document).ready(function ($) {
	//MAIN NAVIGATION
	$('.main-navigation .sub-menu').each(function () {
		$(this).append('<i class="close-main-navigation icon-cancel"></i>');
	});
	$('.main-navigation .menu-item-has-children > a').hover(function () {
		$('.main-navigation .menu-item-has-children .sub-menu').removeClass('visible');
		$(this).next('.sub-menu').addClass('visible');
		$('.main-navigation .menu-item-has-children > a').removeClass('active');
		$(this).addClass('active');
	});
	$('.close-main-navigation').click(function () {
		$('.main-navigation .menu-item-has-children .sub-menu').removeClass('visible');
		$('.main-navigation .menu-item-has-children > a').removeClass('active');
	});
	$('.main-navigation .menu-item-has-children > a').click(function () {
		return false;
	});
	//OWL-HOME CAROUSEL
	$('.owl-home').owlCarousel({
		loop: true,
		nav: true,
		items: 1,
		dots: false,
		autoplay: true,
		autoplayTimeout: 4000,
		autoplayHoverPause: true,
		animateOut: 'fadeOut',
		navContainer: '#owl-home-navigation',
		navText: ["<i class=\"icon-next-bold\"></i>", "<i class=\"icon-next-bold\"></i>"]
	});
	//Toogle mobile footer menu
	$('.mobile-menu ul li.menu-item-has-children > a').click(function () {
		$(this).toggleClass('active');
		$(this).next('.sub-menu').toggle('slow');
		return false
	});
	//Toogle mobile header navigation
	$('.hamburger').click(function () {
		$('.hamburger').toggleClass('is-active');
		$('.mobile-header-navigation').toggle('slow');
		return false
	});
	//Toogle footer menu
	$('.footer-nav ul li.menu-item-has-children > a').click(function () {
		$(this).toggleClass('active');
		$(this).next('.sub-menu').toggle('slow');
		return false
	});
	//Toogle search
	$('.open-search').click(function () {
		$(this).toggleClass('active');
		$('.search-block').toggle('slow');
	});
	//Close search
	$('.close-search-block').click(function () {
		$('.search-block').toggle('slow');
		$('.open-search').toggleClass('active');
	});
	//Toogle right cart
	$('.basket').click(function () {
		$('.xoo-wsc-modal').toggleClass('xoo-wsc-active');
		return false;
	});
	//Кнопка "Наверх"
	//Документация:
	//http://api.jquery.com/scrolltop/
	//http://api.jquery.com/animate/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('#top').fadeIn();
		} else {
			$('#top').fadeOut();
		}
	});
	$("#top").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	//Initialization Fancybox
	$('[data-fancybox]').fancybox({
		animationEffect: 'fade',
		beforeShow: function () {
			$("body *:not(.fancybox-container, .fancybox-container *)").addClass("blur");
		},
		afterClose: function () {
			$("body *:not(.fancybox-container, .fancybox-container *)").removeClass("blur");
		}
	});
	//E-mail Ajax Send
	$('.default-form').submit(function () { //Change
		var th = $(this);
		$.ajax({
			type: 'POST',
			url: '/wp-content/themes/il-pation/mail.php', //Change
			data: th.serialize()
		}).done(function () {
			alert('Сообщение отправлено!');
			$.fancybox.close(); //Закрыть pop-up
			setTimeout(function () {
				// Done Functions
				th.trigger('reset');
			}, 1000);
		});
		return false;
	});
	//Initialization Maskedinput
	$('.phone-input').mask('+7 (999) 999-9999');
	$('input[name="billing_phone"]').mask('+7 (999) 999-9999');
	//Add close option button
	if ($('li.product div').is('.tm-has-options')) {
		$('div.tm-has-options').append('<i class="close-product-option icon-cancel"></i>');
		$('div.tm-has-options').before('<p class="open-product-option" style="display:none;">Дополнительные ингредиенты</p>');
	}
	if ($('.single-product li.product').is('.tm-has-options')) {
		$('.tc-extra-product-options').append('<i class="close-product-option icon-cancel"></i>');
		$('.tc-extra-product-options').before('<p class="open-product-option" style="display:block;">Дополнительные ингредиенты</p>');
	}
	//Close product option
	$('.close-product-option').click(function () {
		$(this).parent().toggle('fast');
		$(this).parent().prev('.open-product-option').show();
	});
	//Open option product
	$('.open-product-option').click(function () {
		$(this).next().toggle('fast');
	});
	$('.products li.tm-has-options .single_add_to_cart_button').on('click', function () {
		if ($(this).is('.custom_add_to_cart')) {
			return true;
		} else {
			$(this).closest('form.cart').next().next('.tm-has-options').toggle('fast');
			$(this).addClass('custom_add_to_cart');
			return false;
		}
	});
	//Update atribute diametr
	$('input[value="40-sm"]').click(function () {
		var thisParent = $(this).closest('li.product');
		thisParent.children('.characteristics').hide();
		thisParent.children('.characteristics_pizza40').show();
	});
	$('input[value="30-sm"]').click(function () {
		var thisParent = $(this).closest('li.product');
		thisParent.children('.characteristics').show();
		thisParent.children('.characteristics_pizza40').hide();
	});
	//Stilization input woo billing form
	$('.woocommerce-billing-fields .form-row input').focus(function () {
		$(this).prev().addClass('active');
	});
	$('.woocommerce-billing-fields .form-row input').each(function () {
		if ($(this).val()) {
			$(this).prev().addClass('active');
		}
	});
	//Time Picker
	var optionsTime = {
		now: "12:00",
		twentyFour: true,
		title: 'Укажите время',
		minutesInterval: 30,
		clearable: true
	};
	$('#billing_myfield12').focus(function () {
		$(this).wickedpicker(optionsTime);
	});
});
